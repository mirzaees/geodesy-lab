# geodesy lab
This container includes most geodesy tools:
[MinSAR](https://github.com/geodesymiami/rsmas_insar), [isce2](https://github.com/isce-framework/isce2), [MintPy](https://github.com/insarlab/MintPy), [pyaps3](https://github.com/AngeliqueBenoit/pyaps3), [FRInGE](https://github.com/isce-framework/fringe), [GMTSAR](https://github.com/gmtsar/gmtsar), [SNAP](http://step.esa.int/downloads/7.0/installers/esa-snap_all_unix_7_0.sh), [Meshroom](https://github.com/alicevision/meshroom/releases)

https://www.unavco.org/software/data-processing/teqc/development/teqc_CentOSLx86_64d.zip

https://geodynamics.org/cig/software/specfem3d

If you are not familiar with containers, read the following documents:

https://containers-at-tacc.readthedocs.io/en/latest

## Prerequisites

setting environment variable:

`export NOTIFICATIONEMAIL=<email address>`

`export JOBSHEDULER_PROJECTNAME=<your project name on HPC>`

`export WORKDIR=<directory with SENTINNEL_ORBITS and SENTINEL_AUX>`

## Build the docker image and deploy locally

First download following zip files into the same directory where Dockerfile exists:

https://www.unavco.org/software/data-processing/teqc/development/teqc_CentOSLx86_64d.zip

https://github.com/alicevision/meshroom/releases/tag/v2019.2.0


Then build the docker container image:

`docker build -t geodesy-lab .`

Run a container:


JupyterLab interface: 

`docker run -d --rm -p 8888:8888 -v $HOME:/home/jovyan/work -v $WORKDIR:/home/jovyan/insarlab -e JUPYTER_LAB_ENABLE=yes --name geodesy-lab geodesy-lab start.sh jupyter lab --LabApp.token=''`

Classic notebook interface: 

`docker run -d --rm -p 8888:8888 -v $HOME:/home/jovyan/work -v $WORKDIR:/home/jovyan/insarlab --name geodesy-lab geodesy-lab start.sh jupyter notebook --NotebookApp.token=''`



## Run a container from the registry:
`docker run -d --rm -p 8888:8888 -v $HOME:/home/jovyan/work -v $WORKDIR:/home/jovyan/insarlab -e JUPYTER_LAB_ENABLE=yes --name geodesy-lab registry.gitlab.com/mirzaees/geodesy-lab start.sh jupyter lab --LabApp.token=''`

## Running with Singularity

https://sylabs.io/singularity-desktop-macos/

`singularity pull geodesy-lab.sif docker://registry.gitlab.com/mirzaees/geodesy-lab`

`singularity exec geodesy-lab.sif process_rsmas.py -h`
