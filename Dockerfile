FROM jupyter/tensorflow-notebook
MAINTAINER Sara Mirzaee "smirzaee@unavco.org"

#############################################
### System Libraries and Dependencies
#############################################
USER root
RUN apt-get update && \
    apt-get install -y ssh wget rsync bzip2 csh vim subversion git autoconf gcc g++ gfortran libmotif-dev libxcb1-dev libfftw3-dev liblapack-dev libhdf5-dev libtiff5-dev libatlas-base-dev libeigen3-dev libgmt-dev gmt gmt-gshhg gmt-gshhg openjdk-8-jdk maven && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN git clone https://github.com/geodesymiami/rsmas_insar.git /usr/local/development/rsmas_insar 
RUN cd /usr/local/development/rsmas_insar && git clone https://github.com/insarlab/MintPy.git sources/MintPy 
RUN cd /usr/local/development/rsmas_insar && git clone https://github.com/bakerunavco/SSARA.git 3rdparty/SSARA 
RUN cd /usr/local/development/rsmas_insar && git clone https://github.com/yunjunz/pyaps3.git 3rdparty/PyAPS/pyaps3 
RUN cd /usr/local/development/rsmas_insar && git clone https://github.com/TACC/launcher.git 3rdparty/launcher 

# RUN cd /usr/local/development/rsmas_insar && git clone https://github.com/mapbox/tippecanoe.git 3rdparty/tippecanoe && make -C 3rdparty/tippecanoe install PREFIX=3rdparty/tippecanoe

# Switch back to jovyan 
USER $NB_UID

RUN conda install --yes --quiet \ 
    ipywidgets \
    dask \ 
    numba \
    sympy \
    netcdf4 \
    lxml \  
    scikit-learn \ 
    scikit-image \ 
    pandas \ 
    pyhdf \ 
    requests \ 
    pyresample \ 
    cartopy \ 
    cdsapi \
    cvxopt \ 
    dask-jobqueue>=0.3 \
    ecCodes \
    pygrib \
    pykdtree \
    pyproj \
    rise \ 
    xlrd \ 
    cython \ 
    gdal>=3.0.0 \ 
    h5py \
    pytest \
    numpy \
    fftw \
    scipy \
    basemap \
    scons \
    cmake \
    armadillo \
    natsort \
    opencv && \
    conda clean --all -f -y && \
    # Activate ipywidgets extension in the environment that runs the notebook server
    jupyter nbextension enable --py widgetsnbextension --sys-prefix && \
    jupyter nbextension enable rise --py --sys-prefix && \
    jupyter lab build --dev-build=False && \
    npm cache clean --force && \
    rm -rf $CONDA_DIR/share/jupyter/lab/staging && \
    rm -rf /home/$NB_USER/.cache/yarn && \
    rm -rf /home/$NB_USER/.node-gyp && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

#############################################
### Jupyter Extensions
#############################################
RUN python3 -m pip install jupyter_nbextensions_configurator jupyter_contrib_nbextensions jupyter_dashboards ipympl jupyterlab_sql psycopg2-binary cesiumpy
RUN conda install hide_code
RUN jupyter contrib nbextension install --user
RUN jupyter nbextensions_configurator enable --user
RUN jupyter nbextension enable --py hide_code
RUN jupyter serverextension enable --py hide_code
# Install the jupyter dashboards.
RUN jupyter dashboards quick-setup --sys-prefix
RUN jupyter nbextension enable jupyter_dashboards --py --sys-prefix
# Install interactive matplotlib
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager
RUN jupyter labextension install jupyter-matplotlib jupyter-threejs jupyterlab-datawidgets
#RUN jupyter labextension install jupyterlab_bokeh@1.0.0 --no-build && \

#############################################
### Libraries and Dependencies
#############################################
RUN python3 -m pip install obspy georinex pyasdf pulsar-client geocoder
RUN conda install -c conda-forge vaex pdal geopy wand jupyter_contrib_nbextensions folium
RUN python3 -m pip install git+https://github.com/Turbo87/utm
RUN python3 -m pip install git+https://github.com/pyrocko/kite
RUN python3 -m pip install git+https://github.com/pyrocko/pyrocko.git

## For MintPy
RUN GEOS_DIR=/opt/conda/lib/ CPATH=/opt/conda/include/ python3 -m pip install git+https://github.com/matplotlib/basemap.git
RUN python3 -m pip install git+https://github.com/tylere/pykml.git

USER root

#############################################
### InSAR Processing and Analysis Software
#############################################

### ISCE ###
RUN ln -s /opt/conda/bin/cython /opt/conda/bin/cython3 # need for compiling, see isce2/README.md
RUN git clone https://github.com/isce-framework/isce2.git /usr/local/isce2
COPY SConfigISCE /usr/local/isce2
RUN cd /usr/local/isce2 && \
    export PYTHONPATH=/usr/local/isce2/configuration && \
    export SCONS_CONFIG_DIR=/usr/local/isce2 && \
    scons install

# FRInGE ###
RUN mkdir /usr/local/development/FRInGE
RUN cd /usr/local/development/FRInGE && mkdir install build
RUN git clone https://github.com/isce-framework/fringe.git /usr/local/development/FRInGE/src/fringe && \
    cd /usr/local/development/FRInGE/src && \
    CXX=g++ cmake -DCMAKE_INSTALL_PREFIX=../install ../src/fringe && \
    make all && make install


### GMTSAR ###
RUN cd /usr/local/development && git clone https://github.com/gmtsar/gmtsar GMTSAR 
RUN cd /usr/local/development/GMTSAR && autoconf && ./configure && make && make install
### SNAP ###
RUN wget -nv -c http://step.esa.int/downloads/7.0/installers/esa-snap_all_unix_7_0.sh && \
    sh esa-snap_all_unix_7_0.sh -q -dir /usr/local/development && \
    rm esa-snap_all_unix_7_0.sh

#############################################
### GNSS
# GNSS-SDR, GPSTk, GipsyX
# georinex, G-nut/anubis, teqc
#############################################

RUN cd /tmp && git clone https://github.com/SGL-UT/GPSTk && \
    mkdir /tmp/GPSTk/build && cd /tmp/GPSTk/build && \
    cmake .. && make && make install
# RUN wget -P /tmp https://www.unavco.org/software/data-processing/teqc/development/teqc_CentOSLx86_64d.zip #teqc_CentOSLx86_64d.zip /tmp
COPY teqc_CentOSLx86_64d.zip /tmp
RUN unzip /tmp/teqc_CentOSLx86_64d.zip -d /usr/local/bin 

### Seismic
# specfem3d https://geodynamics.org/cig/software/specfem3d/ 

#############################################
### SfM, SLAM, point cloud
#############################################

COPY Meshroom-2019.2.0-linux.tar.gz /tmp
RUN tar xf /tmp/Meshroom-2019.2.0-linux.tar.gz -C /usr/local/development

RUN rm -rf /tmp/*

#############################################
### Set up environment variables
#############################################

USER jovyan

ENV WORKDIR /home/jovyan/insarlab
ENV RSMASINSAR_HOME /usr/local/development/rsmas_insar

ENV SSARAHOME $RSMASINSAR_HOME/3rdparty/SSARA
ENV SENTINEL_ORBITS $WORKDIR/S1orbits
ENV SENTINEL_AUX $WORKDIR/S1aux
ENV WEATHER_DIR $WORKDIR/WEATHER
ENV TESTDATA_ISCE $WORKDIR/TESTDATA_ISCE
ENV MINTPY_HOME $RSMASINSAR_HOME/sources/MintPy
ENV OPERATIONS $WORKDIR/OPERATIONS
ENV JOBDIR $WORKDIR/JOBS
ENV LAUNCHER_DIR $RSMASINSAR_HOME/3rdparty/launcher
ENV LAUNCHER_SCHED block 

ENV GMTSAR /usr/local/development/GMTSAR
ENV sharedir $GMTSAR/share/gmtsar

ENV ISCE_ROOT /usr/local
ENV ISCE_HOME $ISCE_ROOT/isce
ENV ISCE_STACK $ISCE_ROOT/isce/contrib/stack
ENV PROJ_LIB /opt/conda/share/proj
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/local/FRInGE/install/lib
ENV OMP_NUM_THREADS 2
ENV CPL_ZIP_ENCODING UTF-8


ENV PYTHONPATH $PYTHONPATH:$ISCE_ROOT:$ISCE_HOME/applications:$ISCE_HOME/components:$MINTPY_HOME:$RSMASINSAR_HOME:$RSMASINSAR_HOME/3rdparty/PyAPS:$RSMASINSAR_HOME/minsar/utils/ssara_ASF:$RSMASINSAR_HOME/sources:/usr/local/FRInGE/install/python

ENV PATH $PATH:$SSARAHOME:$RSMASINSAR_HOME/minsar:$RSMASINSAR_HOME/minsar/utils/ssara_ASF:$ISCE_HOME/bin:$ISCE_HOME/applications:$MINTPY_HOME/mintpy:$MINTPY_HOME/sh:$GMTSAR/bin:/usr/local/pitstk:/usr/local/FRInGE/install/bin:/usr/local/development/Meshroom-2019.2.0

#:$RSMASINSAR_HOME/3rdparty/tippecanoe

